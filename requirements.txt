Django<4
django-markdown>=0.8.4
django-mysql>=2.3.0
isodate>=0.6.0
Markdown>=2.6.11
mysql-connector-python>=8.0.11
mysqlclient>=1.3.12
protobuf>=3.6.0
py-mysql>=1.0
#pyetherpadlite>=0.1
-e git+https://github.com/devjones/PyEtherpadLite.git#egg=PyEtherpadLite
#-e git+git://github.com/devjones/PyEtherpadLite.git#egg=PyEtherpadLite
https://github.com/gijsdeheij/markdown-figures/archive/master.zip
pyparsing>=2.2.0
python-dateutil>=2.7.3
pytz>=2018.4
rdflib>=4.2.2
six>=1.11.0
South>=1.0.2
html5lib
mdx_semanticdata
git+https://github.com/aleray/aa.sniff.git
rdflib_sqlalchemy
django-filer
bs4
bleach
